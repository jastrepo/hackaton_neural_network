import os

import tensorflow as tf
from django.apps import AppConfig

from hackaton_neural_network import settings


class ImageClassificationConfig(AppConfig):
    name = 'image_classification'

    interpreter = tf.lite.Interpreter(model_path=os.path.join(settings.MODELS, 'resnet50.tflite'))
    interpreter.allocate_tensors()
