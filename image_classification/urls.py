from django.urls import path

from .views import ClassifyFileView, ClassifyURLView

urlpatterns = [
    path('file/<filename>', ClassifyFileView.as_view()),
    path('url/', ClassifyURLView.as_view()),
]
