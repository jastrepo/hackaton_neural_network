import numpy as np
import requests
import tensorflow as tf
from PIL import Image
from rest_framework import views
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response

from .apps import ImageClassificationConfig

interpreter = ImageClassificationConfig.interpreter


def classify_image(image):
    image = image.resize((224, 224))

    image_data = np.array(image)
    image_data = image_data[:, :, :3].reshape((1, 224, 224, 3))

    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]
    interpreter.set_tensor(input_index, image_data)
    interpreter.invoke()
    output = interpreter.tensor(output_index)

    predictions = tf.keras.applications.resnet50.decode_predictions(output())
    result = [{'class': class_name, 'confidence': class_confidence}
              for (_, class_name, class_confidence) in predictions[0]]

    return {'top5 classes': result}


class ClassifyFileView(views.APIView):
    parser_classes = [FileUploadParser, ]

    # noinspection PyUnusedLocal
    @staticmethod
    def put(request, filename):
        image_file = request.data['file']
        image = Image.open(image_file)

        return Response(classify_image(image))


class ClassifyURLView(views.APIView):
    @staticmethod
    def post(request):
        image_url = request.data['url']
        image_stream = requests.get(image_url, stream=True, headers={'User-Agent': ''}).raw
        image = Image.open(image_stream, formats=('JPEG',))

        return Response(classify_image(image))
